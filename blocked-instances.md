# List of Mammouth Café Blocked Instances

This is a list of Mastodon/GNU social instances blocked by [mammouth.cafe](https://mammouth.cafe/). If you think an instance has been unfairly blocked, please open an issue and/or a pull request.

We do not usually comment on muted or suspended individual accounts.

## Domain Blocks

| Domain                 | Severity[^1] | Reject media files | Reason                                   |
| ---------------------- | ------------ | ------------------ | ---------------------------------------- |
| shitposter.club        | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| rainbowdash.net        | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| freezepeach.xyz        | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| woofer.alfter.us       | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| social.au2pb.net       | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| gorf.club              | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| sealion.club           | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| gs.smuglo.li           | suspend      | ✔                  | GNU social apparent free speech zone[^2]; sexualized depictions of children (lolicon[^3]) |
| unsafe.space           | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| social.heldsca.la      | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| wrongthink.net         | suspend      | ✔                  | GNU social apparent free speech zone[^2] |
| pawoo.net              | silence      | ✔                  | Sexualized depictions of children (lolicon[^3]) |
| ediot.social           | suspend      | ✔                  | Apparent free speech zone[^2]            |
| gs.archae.me           | suspend      | ✔                  | Apparent free speech zone[^2]            |
| noagendasocial.com     | suspend      | ✔                  | Apparent free speech zone[^2]            |
| memetastic.space       | suspend      | ✔                  | Apparent free speech zone[^2]            |
| maly.io                | suspend      | ✔                  | Apparent free speech zone[^2]            |
| gs.kawa-kun.com        | silence      | ✔                  | Sexualized depictions of children (lolicon[^3]) |
| antitwitter.com        | suspend      | ✔                  | Apparent free speech zone[^2]            |
| admins.town            | silence      |                    | Inter-admin talk that doesn't need to make too much noise in the signals of the public timeline |
| presidentielle.tech    | silence      |                    | Boring political bots                    |
| porntoot.com           | silence      | ✔                  | Unflagged porn                           |
| masutabedon.com        | silence      | ✔                  | Unflagged porn                           |
| social.tchncs.de       | silence      |                    | Enables harassment[^4]                   |
| cybre.space            | silence      |                    | Enables harassment[^4]                   |
| botsin.space           | silence      |                    | Annoying bots                            |
| social.targaryen.house | silence      |                    | Enables harassment[^4]                   |
| social.imirhil.fr      | suspend      | ✔                  | Single-user instance with multiple accounts for no other discernible reason than to circumvent account suspension |
| anitwitter.moe         | suspend      | ✔                  | Apparent free speech zone[^2]            |
| baraag.net             | suspend      | ✔                  | Unflagged porn, graphic violence and similar content |

## Notes

Read our **rules and guidelines** on [mammouth.cafe/about/more](https://mammouth.cafe/about/more), as well as our **terms of service, code of conduct and privacy policy** on [mammouth.cafe/terms](https://mammouth.cafe/terms).

- We are not a "free speech zone"
- Our culture and legislation is different from that of many GNU social sites
- Our local legislation (Switzerland and France) supersedes our own rules and guidelines
- We do not pass judgement on the rules and guidelines (or lack thereof) of other instances and sites
- We prefer to err on the side of caution

## References

- [https://toot.cat/users/polymerwitch/updates/556](https://toot.cat/users/polymerwitch/updates/556)
- [https://toot.cafe/@nolan/2937](https://toot.cafe/@nolan/2937)
- [https://cybre.space/users/chr/updates/2616](https://cybre.space/users/chr/updates/2616)
- [https://mastodon.xyz/users/status/updates/90466](https://mastodon.xyz/users/status/updates/90466)
- [https://mastodon.social/@Murassa/2625739](https://mastodon.social/@Murassa/2625739)
- [https://woofer.alfter.us/conversation/62187#notice-124626](https://woofer.alfter.us/conversation/62187#notice-124626)
- [https://plateia.org/notice/10007](https://plateia.org/notice/10007)
- [https://mammouth.cafe/@dredmorbius/54963](https://mammouth.cafe/@dredmorbius/54963)
- [https://witches.town/users/Alda/updates/125509](https://witches.town/users/Alda/updates/125509)
- [https://cafe.des-blogueurs.org/users/kozlika/updates/3708](https://cafe.des-blogueurs.org/users/kozlika/updates/3708)
- [https://mastodon.social/@GinnyMcQueen/4707770](https://mastodon.social/@GinnyMcQueen/4707770)
  - [https://octodon.social/users/KevinCarson1/updates/98636](https://octodon.social/users/KevinCarson1/updates/98636)
  - [https://witches.town/users/Alda/updates/142842](https://witches.town/users/Alda/updates/142842)
  - [https://mastodon.social/@noelle/4820889](https://mastodon.social/@noelle/4820889)
  - [https://mastodon.social/@noelle/4834389](https://mastodon.social/@noelle/4834389)
  - [https://toot.cat/users/davidsgallant/updates/21120](https://toot.cat/users/davidsgallant/updates/21120)
  - [https://mastodon.social/@WelshPixie/4849364](https://mastodon.social/@WelshPixie/4849364)
  - [https://witches.town/users/Alda/updates/154811](https://witches.town/users/Alda/updates/154811)
- https://glitch.social/about/more

## Credits and prior art

- [https://github.com/tootcafe/blocked-instances](https://github.com/tootcafe/blocked-instances)
- [https://github.com/WitchesTown/InstanceInternals/blob/master/Blocklist.md](https://github.com/WitchesTown/InstanceInternals/blob/master/Blocklist.md)
- [https://archive.fo/zlBjU#selection-695.1-695.17](https://archive.fo/zlBjU#selection-695.1-695.17)

[^1]: silence (or mute) means posts from that instance won't appear in the federated timeline, but you can still see, follow and interact with their accounts; suspended means no interaction and/or federation allowed from our instance.
[^2]: "free speech zone": a place where it's okay to promote, e.g., racist, sexist, xenophobic or oppressive arguments in the name of open discussion; the term is not derogatory.
[^3]: lolicon: sexualized depictions of minors/children.
[^4]: a small number of individual accounts have also been suspended, not necessarily from this same instance.